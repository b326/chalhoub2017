========================
chalhoub2017
========================

.. {# pkglts, doc

.. image:: https://b326.gitlab.io/chalhoub2017/_images/badge_pkging_pip.svg
    :alt: PyPI version
    :target: https://pypi.org/project/chalhoub2017/0.0.2/

.. image:: https://b326.gitlab.io/chalhoub2017/_images/badge_pkging_conda.svg
    :alt: Conda version
    :target: https://anaconda.org/revesansparole/chalhoub2017

.. image:: https://b326.gitlab.io/chalhoub2017/_images/badge_doc.svg
    :alt: Documentation status
    :target: https://b326.gitlab.io/chalhoub2017/

.. image:: https://badge.fury.io/py/chalhoub2017.svg
    :alt: PyPI version
    :target: https://badge.fury.io/py/chalhoub2017

.. #}
.. {# pkglts, glabpkg_dev, after doc

main: |main_build|_ |main_coverage|_

.. |main_build| image:: https://gitlab.com/b326/chalhoub2017/badges/main/pipeline.svg
.. _main_build: https://gitlab.com/b326/chalhoub2017/commits/main

.. |main_coverage| image:: https://gitlab.com/b326/chalhoub2017/badges/main/coverage.svg
.. _main_coverage: https://gitlab.com/b326/chalhoub2017/commits/main
.. #}

Data and formalisms from Chalhoub et al. 2017 paper

